import { User } from '../user/user.entity';
import { Role } from '../role/role.entity';
import { RoleUser } from '../role/role-user.entity';
import { Book } from '../book/book.entity';

export const databaseEntities = [User, Role, RoleUser, Book];
