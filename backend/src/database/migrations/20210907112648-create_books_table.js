'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface
      .createTable('books', {
        id: {
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('uuid_generate_v4()'),
          allowNull: false,
        },
        name: {
          type: Sequelize.STRING(255),
        },
        description: {
          type: Sequelize.STRING(1000),
        },
        author_id: Sequelize.UUID,
        updated_at: Sequelize.DATE,
        created_at: Sequelize.DATE,
      })
      .then(() => {
        queryInterface.addConstraint('books', {
          fields: ['author_id'],
          type: 'foreign key',
          name: 'fkey_book_users',
          references: {
            table: 'users',
            field: 'id',
          },
          onDelete: 'cascade',
          onUpdate: 'cascade',
        });
      });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('books');
  },
};
