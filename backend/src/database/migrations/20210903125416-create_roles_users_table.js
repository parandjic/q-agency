'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface
      .createTable('roles_users', {
        id: {
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('uuid_generate_v4()'),
          allowNull: false,
        },
        role_id: Sequelize.UUID,
        user_id: Sequelize.UUID,
        updated_at: Sequelize.DATE,
        created_at: Sequelize.DATE,
      })
      .then(() => {
        queryInterface.addConstraint('roles_users', {
          fields: ['role_id'],
          type: 'foreign key',
          name: 'fkey_roles',
          references: {
            table: 'roles',
            field: 'id',
          },
          onDelete: 'cascade',
          onUpdate: 'cascade',
        });

        queryInterface.addConstraint('roles_users', {
          fields: ['user_id'],
          type: 'foreign key',
          name: 'fkey_users',
          references: {
            table: 'users',
            field: 'id',
          },
          onDelete: 'cascade',
          onUpdate: 'cascade',
        });
      });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('roles_users');
  },
};
