'use strict';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const { v4: uuidv4 } = require('uuid');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const saltOrRounds = 14;
    const password = await bcrypt.hash('12345678', saltOrRounds);
    //
    const authors = users.map((user) => {
      return {
        id: uuidv4(),
        ...user,
        password,
        created_at: new Date(),
        updated_at: new Date(),
      };
    });

    // insert users
    await queryInterface.bulkInsert('users', authors);

    // insert roles
    const results = await queryInterface.sequelize.query(
      "SELECT * FROM roles WHERE roles.name = 'author'",
    );

    const authorRole = results[0][0];

    const roles_users = authors.map((author) => {
      return { user_id: author.id, role_id: authorRole.id };
    });

    await queryInterface.bulkInsert('roles_users', roles_users);

    // insert books
    const booksInsertArray = books.map((book, index) => {
      const authorArrayIndex = Math.ceil(index % 10);
      return {
        ...book,
        author_id: authors[authorArrayIndex].id,
        created_at: new Date(),
        updated_at: new Date(),
      };
    });

    await queryInterface.bulkInsert('books', booksInsertArray);
  },

  down: async (queryInterface, Sequelize) => {
    // empty books
    await queryInterface.sequelize.query('DELETE FROM books');

    // get roles roles
    const results = await queryInterface.sequelize.query(
      "SELECT * FROM roles WHERE roles.name = 'author'",
    );

    // get author role
    const authorRole = results[0][0];

    // empty added roles_users
    await queryInterface.sequelize.query(
      'DELETE FROM roles_users WHERE roles_users.role_id != ' +
        "'" +
        authorRole.id +
        "'",
    );

    // empty users
    await queryInterface.sequelize.query(
      "DELETE FROM users WHERE users.firstname != 'qagency'",
    );
  },
};

const users = [
  {
    firstname: 'Arthur',
    lastname: 'Harmon',
    email: 'dolor.Donec@purus.edu',
  },
  {
    firstname: 'Perry',
    lastname: 'Keith',
    email: 'justo.faucibus.lectus@Donecsollicitudinadipiscing.edu',
  },
  {
    firstname: 'Jarrod',
    lastname: 'Rice',
    email: 'tincidunt.aliquam@Phasellusfermentum.org',
  },
  {
    firstname: 'Octavius',
    lastname: 'Rasmussen',
    email: 'Fusce.mi.lorem@magna.com',
  },
  {
    firstname: 'Jarrod',
    lastname: 'Pollard',
    email: 'Suspendisse.commodo.tincidunt@placerat.org',
  },
  {
    firstname: 'Breanna',
    lastname: 'Malone',
    email: 'augue@nasceturridiculus.org',
  },
  {
    firstname: 'Shaine',
    lastname: 'Stark',
    email: 'non@lobortistellus.edu',
  },
  {
    firstname: 'Imani',
    lastname: 'Ray',
    email: 'vitae@etarcu.edu',
  },
  {
    firstname: 'Seth',
    lastname: 'Christian',
    email: 'pharetra.felis.eget@senectus.com',
  },
  {
    firstname: 'Chelsea',
    lastname: 'Rutledge',
    email: 'sem.Nulla.interdum@venenatis.com',
  },
];

const books = [
  {
    name: 'ac',
    description:
      'malesuada vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor',
  },
  {
    name: 'diam.',
    description:
      'sagittis semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus,',
  },
  {
    name: 'et, rutrum eu, ultrices sit amet,',
    description:
      'in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque et nunc. Quisque ornare',
  },
  {
    name: 'aptent taciti sociosqu ad litora torquent',
    description:
      'scelerisque scelerisque dui. Suspendisse ac metus vitae velit egestas lacinia.',
  },
  {
    name: 'Proin sed turpis',
    description:
      'mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem,',
  },
  {
    name: 'tempor',
    description:
      'luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis',
  },
  {
    name: 'Nunc',
    description:
      'leo, in lobortis tellus justo sit amet nulla. Donec non justo. Proin',
  },
  {
    name: 'adipiscing fringilla, porttitor vulputate, posuere vulputate,',
    description:
      'lacinia vitae, sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum',
  },
  {
    name: 'mattis velit',
    description:
      'non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum',
  },
  {
    name: 'amet ultricies sem magna nec quam.',
    description:
      'nonummy ut, molestie in, tempus eu, ligula. Aenean euismod mauris eu elit. Nulla facilisi. Sed neque. Sed eget lacus.',
  },
  {
    name: 'In condimentum. Donec',
    description:
      'porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum. Donec',
  },
  {
    name: 'Nam porttitor scelerisque neque. Nullam',
    description:
      'posuere cubilia Curae; Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus.',
  },
  {
    name: 'vitae',
    description:
      'venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies',
  },
  {
    name: 'Vestibulum ante',
    description:
      'neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit',
  },
  {
    name: 'montes,',
    description:
      'Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui augue eu tellus. Phasellus elit pede, malesuada vel,',
  },
  {
    name: 'aliquam eu, accumsan sed, facilisis vitae,',
    description:
      'elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien,',
  },
  {
    name: 'molestie. Sed',
    description:
      'malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus.',
  },
  {
    name: 'dui.',
    description:
      'enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas',
  },
  {
    name: 'est. Nunc ullamcorper, velit in aliquet',
    description:
      'vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed,',
  },
  {
    name: 'parturient montes,',
    description:
      'faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem,',
  },
  {
    name: 'sodales.',
    description:
      'est, vitae sodales nisi magna sed dui. Fusce aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh sit amet orci. Ut',
  },
  {
    name: 'mauris.',
    description:
      'luctus et ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac metus vitae velit egestas lacinia. Sed congue, elit',
  },
  {
    name: 'fermentum vel, mauris.',
    description:
      'leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend, nunc',
  },
  {
    name: 'bibendum sed, est.',
    description:
      'luctus lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel sapien imperdiet ornare. In faucibus. Morbi vehicula. Pellentesque tincidunt tempus',
  },
  {
    name: 'Cras',
    description:
      'Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis',
  },
  {
    name: 'egestas. Fusce aliquet magna a neque.',
    description:
      'eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae sodales nisi magna sed dui. Fusce aliquam, enim nec tempus',
  },
  {
    name: 'Vestibulum accumsan neque et',
    description:
      'erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque et nunc. Quisque ornare tortor at risus. Nunc ac sem ut',
  },
  {
    name: 'ut, sem. Nulla interdum.',
    description:
      'convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse',
  },
  {
    name: 'nisi.',
    description:
      'natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel arcu eu odio tristique pharetra. Quisque ac libero',
  },
  {
    name: 'dolor. Quisque tincidunt',
    description:
      'lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis',
  },
  {
    name: 'dictum',
    description:
      'scelerisque neque sed sem egestas blandit. Nam nulla magna, malesuada vel, convallis in,',
  },
  {
    name: 'neque',
    description:
      'dui. Suspendisse ac metus vitae velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae',
  },
  {
    name: 'mi enim, condimentum',
    description:
      'leo. Morbi neque tellus, imperdiet non, vestibulum nec, euismod in, dolor. Fusce feugiat.',
  },
  {
    name: 'quam a felis ullamcorper',
    description:
      'litora torquent per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel',
  },
  {
    name: 'Proin velit. Sed malesuada',
    description:
      'et risus. Quisque libero lacus, varius et, euismod et, commodo at, libero. Morbi',
  },
  {
    name: 'Nulla dignissim. Maecenas ornare',
    description:
      'nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin',
  },
  {
    name: 'Etiam vestibulum massa',
    description:
      'nulla. Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede',
  },
  {
    name: 'porttitor vulputate,',
    description:
      'congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem.',
  },
  {
    name: 'lobortis, nisi',
    description:
      'eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a',
  },
  {
    name: 'et, commodo at, libero.',
    description:
      'eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed turpis nec mauris',
  },
  {
    name: 'nunc ac',
    description:
      'neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus',
  },
  {
    name: 'enim commodo hendrerit. Donec porttitor tellus',
    description:
      'Donec elementum, lorem ut aliquam iaculis, lacus pede sagittis augue, eu tempor erat',
  },
  {
    name: 'mauris,',
    description:
      'Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum.',
  },
  {
    name: 'gravida non, sollicitudin a, malesuada id,',
    description:
      'gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim consequat purus. Maecenas libero est, congue a, aliquet',
  },
  {
    name: 'Vivamus',
    description:
      'dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique',
  },
  {
    name: 'netus',
    description:
      'lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id',
  },
  {
    name: 'tempus',
    description:
      'metus. Vivamus euismod urna. Nullam lobortis quam a felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed',
  },
  {
    name: 'mattis semper, dui lectus rutrum urna,',
    description:
      'lorem. Donec elementum, lorem ut aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique',
  },
  {
    name: 'enim. Suspendisse aliquet, sem ut cursus',
    description:
      'Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem',
  },
  {
    name: 'lobortis mauris.',
    description:
      'eleifend egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus pede sagittis augue,',
  },
  {
    name: 'Maecenas malesuada fringilla est. Mauris eu',
    description:
      'quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus faucibus leo,',
  },
  {
    name: 'condimentum. Donec at arcu. Vestibulum',
    description:
      'erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam fringilla cursus',
  },
  {
    name: 'mauris sit amet',
    description:
      'eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in, hendrerit',
  },
  {
    name: 'purus ac tellus. Suspendisse',
    description:
      'ornare. Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus',
  },
  {
    name: 'Aenean egestas hendrerit',
    description:
      'lectus ante dictum mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor',
  },
  {
    name: 'lectus',
    description:
      'nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui augue eu tellus. Phasellus elit',
  },
  {
    name: 'ullamcorper viverra.',
    description:
      'dui, semper et, lacinia vitae, sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi metus. Vivamus euismod urna. Nullam lobortis quam a felis',
  },
  {
    name: 'Nunc sed orci',
    description:
      'lobortis quam a felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat.',
  },
  {
    name: 'tristique aliquet. Phasellus fermentum',
    description:
      'cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin',
  },
  {
    name: 'In mi',
    description:
      'morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem egestas blandit. Nam nulla',
  },
  {
    name: 'nibh',
    description:
      'semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum',
  },
  {
    name: 'euismod',
    description:
      'dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui, in sodales elit erat vitae risus. Duis a mi fringilla',
  },
  {
    name: 'gravida. Praesent eu nulla at sem',
    description:
      'senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer',
  },
  {
    name: 'sit amet diam',
    description:
      'diam dictum sapien. Aenean massa. Integer vitae nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse',
  },
  {
    name: 'dolor. Quisque',
    description:
      'Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor',
  },
  {
    name: 'Integer eu lacus. Quisque imperdiet,',
    description:
      'sem semper erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque et nunc. Quisque',
  },
  {
    name: 'lorem lorem,',
    description:
      'Aenean egestas hendrerit neque. In ornare sagittis felis. Donec tempor, est ac',
  },
  {
    name: 'erat eget ipsum.',
    description:
      'ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui, in',
  },
  {
    name: 'lacinia at,',
    description:
      'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse',
  },
  {
    name: 'non nisi. Aenean',
    description:
      'Aliquam tincidunt, nunc ac mattis ornare, lectus ante dictum mi, ac mattis velit justo nec ante. Maecenas mi',
  },
  {
    name: 'non, cursus non,',
    description:
      'lacinia orci, consectetuer euismod est arcu ac orci. Ut semper pretium neque. Morbi quis urna. Nunc quis arcu vel quam dignissim pharetra.',
  },
  {
    name: 'semper',
    description:
      'pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris. Integer sem elit, pharetra',
  },
  {
    name: 'vel, mauris.',
    description:
      'Sed nunc est, mollis non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam',
  },
  {
    name: 'eu tempor erat neque non',
    description:
      'metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget,',
  },
  {
    name: 'purus gravida sagittis. Duis gravida.',
    description:
      'a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida',
  },
  {
    name: 'orci. Ut',
    description: 'Praesent eu dui. Cum sociis natoque penatibus et magnis dis',
  },
  {
    name: 'nulla vulputate dui,',
    description:
      'litora torquent per conubia nostra, per inceptos hymenaeos. Mauris ut',
  },
  {
    name: 'Fusce fermentum fermentum arcu. Vestibulum',
    description:
      'penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa lobortis ultrices.',
  },
  {
    name: 'mauris sapien,',
    description:
      'risus. Duis a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet,',
  },
  {
    name: 'orci tincidunt adipiscing.',
    description:
      'Integer urna. Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque.',
  },
  {
    name: 'lacus',
    description:
      'lorem vitae odio sagittis semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae',
  },
  {
    name: 'elit.',
    description:
      'Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean',
  },
  {
    name: 'sodales purus, in molestie',
    description:
      'mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent',
  },
  {
    name: 'ac arcu.',
    description:
      'sem elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed et libero. Proin mi. Aliquam gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet',
  },
  {
    name: 'Duis elementum, dui quis accumsan',
    description:
      'consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui, in sodales elit erat vitae risus. Duis a mi fringilla mi lacinia mattis. Integer eu lacus.',
  },
  {
    name: 'sem',
    description:
      'magna, malesuada vel, convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer',
  },
  {
    name: 'nonummy',
    description:
      'orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare sagittis felis. Donec tempor, est ac mattis semper, dui lectus rutrum',
  },
  {
    name: 'diam. Sed diam lorem, auctor quis,',
    description:
      'vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris',
  },
  {
    name: 'arcu. Sed eu nibh',
    description:
      'at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada',
  },
  {
    name: 'diam luctus lobortis.',
    description:
      'at arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac',
  },
  {
    name: 'enim commodo hendrerit. Donec',
    description:
      'quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac',
  },
  {
    name: 'sit amet, consectetuer adipiscing elit. Aliquam',
    description:
      'tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec',
  },
  {
    name: 'est arcu ac',
    description:
      'sollicitudin adipiscing ligula. Aenean gravida nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel arcu eu odio tristique pharetra. Quisque',
  },
  {
    name: 'nec tempus scelerisque, lorem ipsum',
    description:
      'sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris',
  },
  {
    name: 'leo, in lobortis tellus justo',
    description:
      'faucibus leo, in lobortis tellus justo sit amet nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis',
  },
  {
    name: 'aliquet molestie',
    description:
      'ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci',
  },
  {
    name: 'Suspendisse aliquet, sem ut cursus luctus,',
    description:
      'a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae sodales nisi',
  },
  {
    name: 'metus sit amet ante.',
    description:
      'hendrerit neque. In ornare sagittis felis. Donec tempor, est ac mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et,',
  },
  {
    name: 'scelerisque scelerisque dui. Suspendisse ac',
    description:
      'sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus.',
  },
  {
    name: 'rhoncus id,',
    description:
      'amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum',
  },
  {
    name: 'vel, convallis in, cursus et,',
    description:
      'sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique',
  },
  {
    name: 'vulputate dui, nec tempus mauris erat',
    description:
      'lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos',
  },
  {
    name: 'sodales purus, in molestie',
    description:
      'pede. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel arcu eu odio',
  },
  {
    name: 'dui lectus rutrum',
    description:
      'fermentum fermentum arcu. Vestibulum ante ipsum primis in faucibus orci luctus',
  },
  {
    name: 'Maecenas iaculis aliquet',
    description:
      'orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus',
  },
  {
    name: 'odio tristique pharetra. Quisque ac libero',
    description:
      'tellus, imperdiet non, vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem',
  },
  {
    name: 'sed leo. Cras vehicula aliquet libero.',
    description:
      'ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl.',
  },
  {
    name: 'ultrices sit amet, risus. Donec nibh',
    description:
      'ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec,',
  },
  {
    name: 'Cras dolor dolor,',
    description:
      'dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis',
  },
  {
    name: 'rhoncus. Proin nisl sem, consequat nec,',
    description:
      'et, rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in, hendrerit consectetuer, cursus et,',
  },
  {
    name: 'libero at auctor ullamcorper, nisl arcu',
    description:
      'felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor',
  },
  {
    name: 'aliquam arcu.',
    description:
      'placerat, orci lacus vestibulum lorem, sit amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque',
  },
  {
    name: 'ante bibendum ullamcorper. Duis cursus, diam',
    description:
      'interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris sit amet',
  },
  {
    name: 'augue, eu tempor erat',
    description:
      'ac libero nec ligula consectetuer rhoncus. Nullam velit dui, semper et, lacinia vitae, sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac',
  },
  {
    name: 'a, dui. Cras pellentesque. Sed',
    description:
      'netus et malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem egestas blandit. Nam nulla magna, malesuada vel, convallis in, cursus',
  },
  {
    name: 'hendrerit. Donec porttitor',
    description:
      'Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu.',
  },
  {
    name: 'non leo. Vivamus nibh dolor,',
    description:
      'id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque',
  },
  {
    name: 'amet, consectetuer adipiscing',
    description:
      'adipiscing ligula. Aenean gravida nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel arcu eu odio tristique pharetra. Quisque ac',
  },
  {
    name: 'diam',
    description: 'Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem,',
  },
  {
    name: 'Phasellus dolor elit, pellentesque a, facilisis',
    description:
      'ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris',
  },
  {
    name: 'dictum eu, eleifend nec,',
    description:
      'nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris sit amet lorem semper',
  },
  {
    name: 'Vivamus molestie dapibus ligula.',
    description:
      'augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae,',
  },
  {
    name: 'dictum sapien. Aenean massa. Integer vitae',
    description:
      'in molestie tortor nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean',
  },
  {
    name: 'sem.',
    description:
      'feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget,',
  },
  {
    name: 'nonummy',
    description:
      'mus. Donec dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec',
  },
  {
    name: 'justo sit amet nulla. Donec',
    description:
      'Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu',
  },
  {
    name: 'eu lacus. Quisque imperdiet, erat nonummy',
    description:
      'semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio.',
  },
  {
    name: 'eleifend nec, malesuada ut,',
    description:
      'aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames',
  },
  {
    name: 'leo.',
    description:
      'commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum',
  },
  {
    name: 'vel nisl. Quisque fringilla euismod',
    description: 'ornare lectus justo eu arcu. Morbi sit amet massa. Quisque',
  },
  {
    name: 'auctor ullamcorper, nisl arcu iaculis enim,',
    description:
      'fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin',
  },
  {
    name: 'luctus felis purus ac tellus.',
    description:
      'Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat',
  },
  {
    name: 'est mauris, rhoncus id, mollis nec,',
    description:
      'vitae risus. Duis a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit',
  },
  {
    name: 'mollis. Integer tincidunt',
    description:
      'faucibus leo, in lobortis tellus justo sit amet nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis',
  },
  {
    name: 'velit dui, semper',
    description:
      'ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in',
  },
  {
    name: 'lorem ut aliquam iaculis, lacus pede',
    description:
      'est, mollis non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate',
  },
  {
    name: 'amet orci. Ut',
    description:
      'orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer',
  },
  {
    name: 'primis in faucibus orci luctus',
    description:
      'enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum.',
  },
  {
    name: 'quis,',
    description:
      'magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus',
  },
  {
    name: 'pellentesque, tellus',
    description:
      'nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis.',
  },
  {
    name: 'morbi tristique senectus',
    description:
      'tellus, imperdiet non, vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit eget laoreet posuere, enim nisl',
  },
  {
    name: 'Curabitur',
    description:
      'ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin',
  },
  {
    name: 'metus. In lorem.',
    description:
      'penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec',
  },
  {
    name: 'feugiat',
    description:
      'odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu',
  },
  {
    name: 'et',
    description:
      'Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum',
  },
  {
    name: 'Nulla interdum. Curabitur dictum.',
    description:
      'aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy',
  },
  {
    name: 'egestas. Fusce aliquet magna',
    description:
      'Integer vitae nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim.',
  },
  {
    name: 'tempus eu,',
    description:
      'in, cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat.',
  },
  {
    name: 'sit amet, consectetuer adipiscing elit.',
    description:
      'Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec elementum, lorem ut aliquam iaculis,',
  },
  {
    name: 'posuere, enim nisl elementum purus,',
    description:
      'Mauris vel turpis. Aliquam adipiscing lobortis risus. In mi pede, nonummy ut, molestie in, tempus eu, ligula. Aenean euismod mauris eu elit. Nulla facilisi. Sed neque. Sed eget lacus. Mauris',
  },
  {
    name: 'parturient montes, nascetur ridiculus mus.',
    description:
      'elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae,',
  },
  {
    name: 'mollis non, cursus non, egestas a,',
    description:
      'orci. Ut semper pretium neque. Morbi quis urna. Nunc quis arcu vel quam dignissim pharetra. Nam ac nulla. In tincidunt congue turpis. In condimentum. Donec at arcu. Vestibulum',
  },
  {
    name: 'sapien molestie orci tincidunt adipiscing. Mauris',
    description:
      'dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo sit amet nulla. Donec non',
  },
  {
    name: 'convallis est, vitae sodales nisi magna',
    description:
      'pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris. Integer sem',
  },
  {
    name: 'pede. Cum sociis natoque penatibus et',
    description:
      'in faucibus orci luctus et ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac metus vitae',
  },
  {
    name: 'eu nulla',
    description:
      'tempor, est ac mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet,',
  },
  {
    name: 'gravida. Praesent',
    description:
      'ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu,',
  },
  {
    name: 'sit amet,',
    description:
      'molestie in, tempus eu, ligula. Aenean euismod mauris eu elit. Nulla facilisi. Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum',
  },
  {
    name: 'ultrices. Duis volutpat nunc sit',
    description:
      'cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida',
  },
  {
    name: 'tempor arcu. Vestibulum',
    description:
      'vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti',
  },
  {
    name: 'dui',
    description:
      'ac ipsum. Phasellus vitae mauris sit amet lorem semper auctor. Mauris vel',
  },
  {
    name: 'semper erat, in consectetuer ipsum nunc',
    description:
      'rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis',
  },
  {
    name: 'eget nisi',
    description:
      'ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed turpis',
  },
  {
    name: 'risus. Donec egestas.',
    description:
      'massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam. Duis mi enim, condimentum eget, volutpat',
  },
  {
    name: 'Curabitur consequat,',
    description:
      'eu sem. Pellentesque ut ipsum ac mi eleifend egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec elementum,',
  },
  {
    name: 'vulputate, nisi sem semper erat, in',
    description:
      'ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent',
  },
  {
    name: 'Donec fringilla. Donec feugiat',
    description:
      'aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat',
  },
  {
    name: 'Morbi metus. Vivamus euismod urna.',
    description:
      'Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim,',
  },
  {
    name: 'Nullam lobortis quam a felis ullamcorper',
    description:
      'magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non lorem vitae odio sagittis semper. Nam tempor',
  },
  {
    name: 'sem',
    description:
      'molestie tortor nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare sagittis felis. Donec tempor, est ac mattis',
  },
  {
    name: 'non,',
    description:
      'dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed turpis nec mauris blandit mattis. Cras eget',
  },
  {
    name: 'lectus quis massa. Mauris vestibulum, neque',
    description:
      'adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque',
  },
  {
    name: 'Nam ac',
    description:
      'congue turpis. In condimentum. Donec at arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel pede',
  },
  {
    name: 'tempor bibendum. Donec felis',
    description:
      'nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque',
  },
  {
    name: 'Curabitur',
    description:
      'Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non lorem vitae',
  },
  {
    name: 'nisi.',
    description:
      'sit amet ante. Vivamus non lorem vitae odio sagittis semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae nibh. Donec est',
  },
  {
    name: 'risus. Nulla eget metus',
    description:
      'enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo.',
  },
  {
    name: 'eget, ipsum. Donec sollicitudin adipiscing',
    description:
      'Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper,',
  },
  {
    name: 'velit. Aliquam nisl.',
    description:
      'leo, in lobortis tellus justo sit amet nulla. Donec non justo. Proin non massa',
  },
  {
    name: 'Aliquam vulputate',
    description:
      'Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem,',
  },
  {
    name: 'Duis a mi fringilla mi',
    description:
      'Vivamus nibh dolor, nonummy ac, feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu,',
  },
  {
    name: 'Nullam ut nisi',
    description:
      'Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent',
  },
  {
    name: 'non magna.',
    description:
      'quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna.',
  },
  {
    name: 'turpis non enim.',
    description:
      'magna. Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam',
  },
  {
    name: 'eget nisi dictum augue',
    description:
      'Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at',
  },
  {
    name: 'cursus et, magna.',
    description:
      'metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor',
  },
  {
    name: 'magna. Cras convallis convallis dolor.',
    description:
      'lorem ipsum sodales purus, in molestie tortor nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie',
  },
  {
    name: 'Praesent eu nulla at sem',
    description:
      'In mi pede, nonummy ut, molestie in, tempus eu, ligula. Aenean euismod mauris eu elit. Nulla facilisi. Sed',
  },
  {
    name: 'mauris, aliquam',
    description:
      'tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer',
  },
  {
    name: 'mollis nec, cursus a,',
    description:
      'hendrerit id, ante. Nunc mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula eu enim. Etiam imperdiet dictum magna. Ut tincidunt',
  },
  {
    name: 'elit, a feugiat tellus lorem eu',
    description:
      'purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit',
  },
  {
    name: 'lobortis risus. In',
    description:
      'sem. Pellentesque ut ipsum ac mi eleifend egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec elementum, lorem ut',
  },
  {
    name: 'amet, dapibus id, blandit',
    description:
      'a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae sodales nisi magna sed dui. Fusce aliquam, enim nec',
  },
  {
    name: 'feugiat. Lorem ipsum dolor sit amet,',
    description:
      'penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel nisl. Quisque fringilla euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris',
  },
  {
    name: 'urna. Vivamus molestie dapibus',
    description:
      'aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus nisi.',
  },
  {
    name: 'gravida sit amet, dapibus id, blandit',
    description:
      'nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris sit amet lorem semper auctor. Mauris vel turpis. Aliquam',
  },
  {
    name: 'aliquam eu, accumsan sed,',
    description:
      'Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis,',
  },
  {
    name: 'pede.',
    description:
      'natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel nisl.',
  },
  {
    name: 'mauris blandit mattis. Cras eget nisi',
    description:
      'lorem ut aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam',
  },
  {
    name: 'ornare, libero at auctor ullamcorper,',
    description:
      'rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus',
  },
];
