'use strict';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const { v4: uuidv4 } = require('uuid');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const saltOrRounds = 14;
    const password = await bcrypt.hash('12345678', saltOrRounds);

    const admin = {
      id: uuidv4(),
      firstname: 'q-agency',
      lastname: 'q-agency',
      email: 'admin@q-agency.com',
      password,
      created_at: new Date(),
      updated_at: new Date(),
    };

    await queryInterface.bulkInsert('users', [admin]);

    const results = await queryInterface.sequelize.query(
      "SELECT * FROM roles WHERE roles.name = 'admin'",
    );

    const adminRole = results[0][0];

    await queryInterface.bulkInsert('roles_users', [
      {
        role_id: adminRole.id,
        user_id: admin.id,
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(
      "DELETE FROM users WHERE email = 'admin@q-agency.com'",
    );
  },
};
