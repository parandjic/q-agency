import {
  BelongsTo,
  Column,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { User } from '../user/user.entity';
import { ApiProperty } from '@nestjs/swagger';

@Table({
  tableName: 'books',
  timestamps: true,
  paranoid: false,
  underscored: true,
})
export class Book extends Model {
  @ApiProperty()
  @Column({
    primaryKey: true,
    autoIncrement: true,
  })
  id: string;

  @ApiProperty()
  @Column
  name: string;

  @ApiProperty()
  @Column
  description: string;

  @ApiProperty()
  @ForeignKey(() => User)
  @Column
  authorId: number;

  @BelongsTo(() => User)
  author: User;
}
