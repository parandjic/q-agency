import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { BookService } from './book.service';
import { CreateBookDTO } from './dto/create-book.dto';
import { Book } from './book.entity';
import { JwtAuthGuard } from '../auth/jwt/jwt-auth.guard';
import { RoleGuard } from '../role/role.guard';
import { Roles } from '../role/role.decorator';
import { use } from 'passport';
import { GetManyAndCountBooksDTO } from './dto/get-many-and-count-books.dto';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';

@UseGuards(JwtAuthGuard, RoleGuard)
@ApiTags('Book')
@ApiBearerAuth('JWT-auth')
@Controller('book')
export class BookController {
  constructor(private bookService: BookService) {}

  @ApiOperation({ summary: 'Get Many books' })
  @ApiOkResponse({ type: [Book] })
  @Get()
  async getMany(
    @Req() request,
    @Query('page') page: number,
  ): Promise<GetManyAndCountBooksDTO> {
    const { user } = request;
    return this.bookService.getMany(user, page);
  }

  @ApiOperation({ summary: 'Get books by id' })
  @ApiOkResponse({ type: Book })
  @Get('/:id')
  async getById(@Req() request, @Param('id') id: string): Promise<Book> {
    const { user } = request;
    return this.bookService.getOneById(user, id);
  }

  @ApiOperation({ summary: 'Create book @author only' })
  @ApiCreatedResponse()
  @Post()
  @Roles('author')
  async create(@Req() request, @Body() payload: CreateBookDTO): Promise<Book> {
    const { user } = request;
    return this.bookService.create(payload, user.id);
  }
}
