import { Module } from '@nestjs/common';
import { BookController } from './book.controller';
import { BookService } from './book.service';
import { DatabaseModule } from '../database/database.module';

@Module({
  controllers: [BookController],
  providers: [BookService],
  imports: [DatabaseModule],
})
export class BookModule {}
