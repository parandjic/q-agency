export const bookErrors = {
  BOOK_NOT_FOUND: {
    message: 'Book not found!',
    key: 'BOOK_NOT_FOUND',
  },
};
