import { Book } from '../book.entity';

export class GetManyAndCountBooksDTO {
  count: number;
  books: Book[];
}
