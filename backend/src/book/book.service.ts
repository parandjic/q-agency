import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Book } from './book.entity';
import { CreateBookDTO } from './dto/create-book.dto';
import { User } from '../user/user.entity';
import { GetManyAndCountBooksDTO } from './dto/get-many-and-count-books.dto';
import * as Joi from 'joi';
import { bookErrors } from './book.errors';

@Injectable()
export class BookService {
  constructor(
    @InjectModel(Book)
    private book: typeof Book,
  ) {}

  /**
   * Get Many books
   *
   * @param jwtUser
   * @param page
   */
  async getMany(jwtUser, page: number): Promise<GetManyAndCountBooksDTO> {
    const isAuthor = jwtUser.roles.some((role) => {
      if (role.name === 'author') {
        return true;
      }
    });

    let where = {};

    if (isAuthor) {
      where = {
        authorId: jwtUser.id,
      };
    }

    const limit = 10;
    const offset = page ? Math.abs((page - 1) * limit) : 0;

    const { count, rows: books } = await this.book.findAndCountAll({
      include: [
        {
          model: User,
          as: 'author',
          attributes: ['id', 'firstname', 'lastname', 'email'],
        },
      ],
      where,
      limit,
      offset,
      distinct: true,
    });

    return { count, books };
  }

  async getOneById(jwtUser, bookId: string): Promise<Book> {
    const validationResult = Joi.string().uuid().validate(bookId);

    if (validationResult.error) {
      throw new NotFoundException(bookErrors.BOOK_NOT_FOUND);
    }

    const isAdmin = jwtUser.roles.some((role) => {
      if (role.name === 'admin') {
        return true;
      }
    });

    const where = {
      id: bookId,
      authorId: jwtUser.id,
    };

    if (isAdmin) {
      delete where.authorId;
    }

    const book = await this.book.findOne({
      where,
    });

    if (!book) {
      throw new NotFoundException(bookErrors.BOOK_NOT_FOUND);
    }

    return book;
  }

  /**
   * Create Book
   *
   * @param payload
   * @param authorId
   */
  async create(payload: CreateBookDTO, authorId): Promise<Book> {
    return await this.book
      .create({
        ...payload,
        authorId,
      })
      .catch((error) => {
        console.log(error);
        throw new InternalServerErrorException();
      });
  }
}
