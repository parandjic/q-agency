import {
  BelongsToMany,
  Column,
  HasMany,
  Model,
  Table,
} from 'sequelize-typescript';
import { Role } from '../role/role.entity';
import { RoleUser } from '../role/role-user.entity';
import { Book } from '../book/book.entity';
import { ApiProperty } from '@nestjs/swagger';

@Table({
  tableName: 'users',
  timestamps: true,
  paranoid: false,
  underscored: true,
})
export class User extends Model {
  @ApiProperty()
  @Column({
    primaryKey: true,
    autoIncrement: true,
  })
  id: string;

  @ApiProperty({ example: 'Johan' })
  @Column
  firstname: string;

  @ApiProperty({ example: 'Smith' })
  @Column
  lastname: string;

  @ApiProperty({ example: 'johan.smith@gmail.com' })
  @Column
  email: string;

  @ApiProperty({ example: '12345678' })
  @Column
  password: string;

  @ApiProperty()
  @BelongsToMany(() => Role, () => RoleUser)
  roles: Role[];

  @ApiProperty({ type: Book })
  @HasMany(() => Book, 'authorId')
  books: Book;
}
