import { ApiProperty } from '@nestjs/swagger';

export class CreateAuthorDTO {
  @ApiProperty({ example: 'Johan' })
  firstname: string;

  @ApiProperty({ example: 'Smith' })
  lastname: string;

  @ApiProperty({ example: '12345678' })
  password: string;

  @ApiProperty({ example: 'johan.smith@gmail.com' })
  email: string;
}
