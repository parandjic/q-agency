class UserErrorDTO {
  message: string;
  key: string;
  details?: any;
}
