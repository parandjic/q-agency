import { User } from '../user.entity';
import { ApiProperty } from '@nestjs/swagger';

export class GetManyUsersDTO {
  @ApiProperty()
  count: number;
  @ApiProperty()
  users: User[];
}
