export class UpdateUserDTO {
  firstname: string;
  lastname: string;
  email: string;
}
