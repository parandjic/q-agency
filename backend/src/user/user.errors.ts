const userErrors = {
  EMAIL_ALREADY_EXIST: {
    message: 'Email address already taken',
    key: 'EMAIL_ALREADY_EXIST',
  },
  VALIDATION_FAILED: {
    message: 'Validation failed',
    key: 'VALIDATION_FAILED',
  },
  USER_NOT_FOUND: {
    message: 'User not found',
    key: 'USER_NOT_FOUND',
  },
};

export function handleUserError(
  errorType: string,
  errorDetails: any = null,
): UserErrorDTO {
  const error = userErrors[errorType];

  if (errorDetails) {
    error.details = errorDetails;
  }

  return error;
}
