import * as Joi from 'joi';

export const createUserValidation = Joi.object({
  firstname: Joi.string()
    .min(1)
    .max(50)
    .regex(/^[^*|:<>[\]{}\\()\-;@&$+0-9]+$/)
    .required(),

  lastname: Joi.string()
    .min(1)
    .max(50)
    .regex(/^[^*|:<>[\]{}\\()\-;@&$+0-9]+$/)
    .required(),

  email: Joi.string().email().required(),

  password: Joi.string().min(7).required(),
});
