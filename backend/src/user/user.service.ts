import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User } from './user.entity';
import { createUserValidation } from './validation/create-user.validation';
import { handleUserError } from './user.errors';
import { CreateUserDTO } from './dto/create-user.dto';
import * as bcrypt from 'bcrypt';
import { UpdateUserDTO } from './dto/update-user.dto';
import * as Joi from 'joi';
import { updateUserValidation } from './validation/update-user.validation';
import { GetManyUsersDTO } from './dto/get-many-users.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User)
    private user: typeof User,
  ) {}

  /**
   * Get one user
   *
   * @param params
   */
  async getOne(params: any): Promise<User> {
    return this.user.findOne(params);
  }

  /**
   * Get many users
   *
   * @param params
   */
  async getMany(params): Promise<GetManyUsersDTO> {
    const { count, rows: users } = await this.user.findAndCountAll(params);

    return { count, users };
  }

  /**
   * Create User
   *
   * @param payload
   */
  async create(payload: CreateUserDTO): Promise<User> {
    const validationResult = createUserValidation.validate(payload, {
      abortEarly: false,
    });

    if (validationResult.error) {
      throw new BadRequestException(
        handleUserError('VALIDATION_FAILED', validationResult.error.details),
      );
    }

    // hash password
    const saltOrRounds = 14;
    payload.password = await bcrypt.hash(payload.password, saltOrRounds);

    // create user
    try {
      return await this.user.create(payload);
    } catch (error) {
      // TODO: [Feature] implement error handler service...
      if (error?.parent?.code == 23505) {
        throw new BadRequestException(handleUserError('EMAIL_ALREADY_EXIST'));
      }
      throw new InternalServerErrorException();
    }
  }

  /**
   * Update User
   *
   * @param id
   * @param payload
   */
  async update(id: string, payload: UpdateUserDTO): Promise<User> {
    const validationResult = updateUserValidation.validate(payload, {
      abortEarly: false,
    });

    if (validationResult.error) {
      throw new BadRequestException(
        handleUserError('VALIDATION_FAILED', validationResult.error.details),
      );
    }

    let results = [];

    try {
      results = await this.user.update(payload, {
        where: { id },
        returning: true,
      });
    } catch (error) {
      // TODO: [Feature] implement error handler service...
      if (error?.parent?.code == 23505) {
        throw new BadRequestException(handleUserError('EMAIL_ALREADY_EXIST'));
      }
      throw new InternalServerErrorException();
    }

    if (!results[0]) {
      throw new NotFoundException(handleUserError('USER_NOT_FOUND'));
    }

    const user: User = results[1][0].dataValues;

    delete user.password;

    return user;
  }

  /**
   * Delete User
   *
   * @param id
   */
  async delete(id: string): Promise<void> {
    const validationResult = Joi.string().uuid().validate(id);

    if (validationResult.error) {
      throw new NotFoundException(handleUserError('USER_NOT_FOUND'));
    }

    const result = await this.user.destroy({ where: { id } });

    if (!result) {
      throw new NotFoundException(handleUserError('USER_NOT_FOUND'));
    }
  }
}
