export default async () => {
  const env = process.env.NODE_ENV || 'local';
  const configFilePath = __dirname + '/../' + env + '.env.json';
  return await import(configFilePath);
};
