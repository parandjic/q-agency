export const authorErrors = {
  AUTHOR_NOT_FOUND: {
    message: 'Author not found',
    key: 'AUTHOR_NOT_FOUND',
  },
};
