import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt/jwt-auth.guard';
import { RoleGuard } from '../role/role.guard';
import { Roles } from '../role/role.decorator';
import { CreateAuthorDTO } from '../user/dto/create-author.dto';
import { User } from '../user/user.entity';
import { AuthorService } from './author.service';
import { UpdateUserDTO } from '../user/dto/update-user.dto';
import {
  ApiBearerAuth,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { GetManyUsersDTO } from '../user/dto/get-many-users.dto';

@ApiTags('Author')
@ApiBearerAuth('JWT-auth')
@UseGuards(JwtAuthGuard, RoleGuard)
@Roles('admin')
@Controller('author')
export class AuthorController {
  constructor(private authorService: AuthorService) {}

  @ApiOperation({ summary: 'Get many authors @admin only' })
  @ApiOkResponse({ type: GetManyUsersDTO })
  @ApiQuery({ type: Number, name: 'page', required: false })
  @Get()
  async getMany(@Query() query: { page: number }) {
    return this.authorService.getMany(query);
  }

  @ApiOperation({ summary: 'Create author @admin only' })
  @ApiCreatedResponse({ type: User })
  @ApiConflictResponse()
  @Post()
  async createAuthor(@Body() payload: CreateAuthorDTO): Promise<User> {
    return this.authorService.create(payload);
  }

  @ApiOperation({ summary: 'Create author @admin only' })
  @ApiOkResponse({ type: User })
  @ApiNotFoundResponse()
  @Patch('/:id')
  async update(
    @Body() payload: UpdateUserDTO,
    @Param('id') id: string,
  ): Promise<User> {
    return this.authorService.update(id, payload);
  }

  @ApiOperation({ summary: 'Delete author @admin only' })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @Delete('/:id')
  @HttpCode(204)
  async delete(@Param('id') id: string): Promise<void> {
    await this.authorService.delete(id);
  }
}
