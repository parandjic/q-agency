import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UserService } from '../user/user.service';
import { CreateAuthorDTO } from '../user/dto/create-author.dto';
import { User } from '../user/user.entity';
import { RoleService } from '../role/role.service';
import { UpdateUserDTO } from '../user/dto/update-user.dto';
import * as Joi from 'joi';
import { handleUserError } from '../user/user.errors';
import { Role } from '../role/role.entity';
import { authorErrors } from './author.errors';
import { GetManyUsersDTO } from '../user/dto/get-many-users.dto';

@Injectable()
export class AuthorService {
  constructor(
    private userService: UserService,
    private roleService: RoleService,
  ) {}

  /**
   * Get many authors
   *
   * @param query
   */
  async getMany(query: { page: number }): Promise<GetManyUsersDTO> {
    const page = Number.isInteger(parseInt(String(query.page)))
      ? query.page
      : 1;
    const params = {
      attributes: ['id', 'firstname', 'lastname', 'email', 'createdAt'],
      limit: 10,
      offset: (Math.abs(page) - 1) * 10,
      order: [['createdAt', 'DESC']],
      include: [{ model: Role, as: 'roles', where: { name: 'author' } }],
    };

    return this.userService.getMany(params);
  }

  /**
   * Create Author
   *
   * @param payload
   */
  async create(payload: CreateAuthorDTO): Promise<User> {
    const author = await this.userService.create(payload);

    // add role author
    await this.roleService.setUserRole('author', author.id);

    return author;
  }

  /**
   * Update Author
   *
   * @param payload
   * @param id
   */
  async update(id: string, payload: UpdateUserDTO): Promise<User> {
    const uuidValidation = Joi.string().uuid().validate(id);

    if (uuidValidation.error) {
      throw new NotFoundException(authorErrors.AUTHOR_NOT_FOUND);
    }

    const author: User = await this.userService.getOne({
      where: { id },
      include: [{ model: Role, as: 'roles', where: { name: 'author' } }],
    });

    if (!author) {
      throw new NotFoundException(authorErrors.AUTHOR_NOT_FOUND);
    }

    return this.userService.update(id, payload);
  }

  /**
   * Delete author
   *
   * @param id
   */
  async delete(id: string): Promise<void> {
    const author: User = await this.userService.getOne({
      where: { id },
      include: [{ model: Role, as: 'roles', where: { name: 'author' } }],
    });

    if (!author) {
      throw new NotFoundException(handleUserError('USER_NOT_FOUND'));
    }

    await this.userService.delete(author.id);
  }
}
