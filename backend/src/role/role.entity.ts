import { BelongsToMany, Column, Model, Table } from 'sequelize-typescript';
import { RoleUser } from './role-user.entity';
import { User } from '../user/user.entity';
import { ApiProperty } from '@nestjs/swagger';

@Table({
  tableName: 'roles',
  timestamps: false,
  paranoid: false,
  underscored: true,
})
export class Role extends Model {
  @ApiProperty()
  @Column({
    primaryKey: true,
    autoIncrement: true,
  })
  id: string;

  @ApiProperty()
  @Column
  name: string;

  @ApiProperty()
  @BelongsToMany(() => User, () => RoleUser)
  roles: User[];
}
