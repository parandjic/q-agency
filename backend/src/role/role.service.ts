import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Role } from './role.entity';
import { RoleUser } from './role-user.entity';

@Injectable()
export class RoleService {
  constructor(
    @InjectModel(Role)
    private role: typeof Role,
    @InjectModel(RoleUser)
    private roleUser: typeof RoleUser,
  ) {}

  async setUserRole(roleName: string, userId: string): Promise<RoleUser> {
    const role: Role = await this.role.findOne({
      where: { name: roleName },
    });

    if (!role) {
      // TODO: [Feature] add proper error handling
      throw new BadRequestException('role not found');
    }

    const createRoleUser: Partial<RoleUser> = {
      userId,
      roleId: role.id,
    };

    try {
      return await this.roleUser.create(createRoleUser);
    } catch (error) {
      // TODO: [Feature] add error message
      throw new BadRequestException();
    }
  }
}
