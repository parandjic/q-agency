import { Column, ForeignKey, Model, Table } from 'sequelize-typescript';
import { Role } from './role.entity';
import { User } from '../user/user.entity';

@Table({
  tableName: 'roles_users',
  timestamps: true,
  paranoid: false,
  underscored: true,
})
export class RoleUser extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
  })
  id: string;

  @ForeignKey(() => Role)
  @Column
  roleId: string;

  @ForeignKey(() => User)
  @Column
  userId: string;
}
