import { Module } from '@nestjs/common';
import { RoleService } from './role.service';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  exports: [RoleService],
  providers: [RoleService],
})
export class RoleModule {}
