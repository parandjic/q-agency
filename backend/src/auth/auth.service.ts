import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { LoginDTO } from './dto/login.dto';
import * as bcrypt from 'bcrypt';
import { authErrors } from './auth.errors';
import { LoginResponseDTO } from './dto/login-response.dto';
import { JwtDTO } from './dto/jwt.dto';
import { CreateJwtDTO } from './dto/create-jwt.dto';
import { Role } from '../role/role.entity';
import { User } from '../user/user.entity';
import { loginValidation } from './validation/login.validation';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UserService,
    private jwtService: JwtService,
  ) {}

  async login(payload: LoginDTO): Promise<LoginResponseDTO> {
    const validationResult = loginValidation.validate(payload, {
      abortEarly: false,
    });

    if (validationResult.error) {
      throw new BadRequestException(authErrors.EMAIL_OR_PASSWORD_INCORRECT);
    }

    const user: User = await this.usersService.getOne({
      where: { email: payload.email },
      include: [{ model: Role, as: 'roles', attributes: ['name'] }],
    });

    if (user) {
      const isMatch: boolean = await bcrypt.compare(
        payload.password,
        user.password,
      );
      if (isMatch) {
        return {
          auth: this.generateJWT(
            { id: user.id, email: user.email, roles: user.roles, type: 'auth' },
            // TODO: lower time of jwt to 15 min after proper implementation of refresh token
            60 * 24 * 10,
          ),
          refresh: this.generateJWT(
            {
              id: user.id,
              email: user.email,
              roles: user.roles,
              type: 'refresh',
            },
            60 * 24 * 30,
          ),
        };
      }
    }

    throw new NotFoundException(authErrors.EMAIL_OR_PASSWORD_INCORRECT);
  }

  refreshToken(payload: { token: string }): LoginResponseDTO {
    try {
      this.jwtService.verify(payload.token);
    } catch (e) {
      throw new UnauthorizedException(authErrors.TOKEN_EXPIRED);
    }

    const tokenDecoded: any = this.jwtService.decode(payload.token);

    if (tokenDecoded.typ !== 'refresh') {
      throw new UnauthorizedException(authErrors.WRONG_TOKEN_TYPE_PROVIDED);
    }

    // TODO: [FEATURE] Implement refresh token being able to be used only once

    return {
      auth: this.generateJWT(
        {
          id: tokenDecoded.id,
          email: tokenDecoded.email,
          roles: tokenDecoded.roles,
          type: 'auth',
        },
        15,
      ),
      refresh: this.generateJWT(
        {
          id: tokenDecoded.id,
          email: tokenDecoded.email,
          roles: tokenDecoded.roles,
          type: 'refresh',
        },
        60 * 24 * 30,
      ),
    };
  }

  generateJWT(payload: CreateJwtDTO, expiresIn: number): string {
    const expiresDate = new Date();
    expiresDate.setTime(expiresDate.getTime() + expiresIn * 60000);

    const jwtData: JwtDTO = {
      sub: payload.id,
      email: payload.email,
      roles: payload.roles,
      typ: payload.type,
      exp: expiresDate.getTime() / 1000,
    };

    return this.jwtService.sign(jwtData);
  }
}
