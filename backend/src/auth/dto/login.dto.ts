import { ApiProperty } from '@nestjs/swagger';

export class LoginDTO {
  @ApiProperty({ example: 'example@example.com', required: true })
  email: string;
  @ApiProperty({ example: '12345678', required: true })
  password: string;
}
