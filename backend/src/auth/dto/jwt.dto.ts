export class JwtDTO {
  sub: string;
  email: string;
  exp: number;
  typ: string;
  roles: Role[];
}

class Role {
  name: string;
}
