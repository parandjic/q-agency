export class CreateJwtDTO {
  id: string;
  email: string;
  type: string;
  roles: Role[];
}

class Role {
  name: string;
}
