import { Body, Controller, Post, Req } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDTO } from './dto/login.dto';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { LoginResponseDTO } from './dto/login-response.dto';
import { RefreshTokenDTO } from './dto/refresh-token.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOperation({ summary: 'Login via email and password' })
  @ApiOkResponse({ type: LoginResponseDTO })
  @Post('login')
  async login(
    @Body() payload: LoginDTO,
    @Req() request,
  ): Promise<LoginResponseDTO> {
    return this.authService.login(payload);
  }

  @ApiOperation({ summary: 'Refresh login via refresh token' })
  @ApiOkResponse({ type: LoginResponseDTO })
  @Post('refresh')
  async refreshToken(
    @Body() payload: RefreshTokenDTO,
  ): Promise<LoginResponseDTO> {
    return this.authService.refreshToken(payload);
  }
}
