export const authErrors = {
  EMAIL_OR_PASSWORD_INCORRECT: {
    message: 'Email or password incorrect!',
    key: 'EMAIL_OR_PASSWORD_INCORRECT',
  },

  TOKEN_EXPIRED: {
    message: 'You token expired',
    key: 'TOKEN_EXPIRED',
  },

  WRONG_TOKEN_TYPE_PROVIDED: {
    message: 'Wrong token type provided!',
    key: 'WRONG_TOKEN_TYPE_PROVIDED',
  },
};
