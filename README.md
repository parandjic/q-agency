# Q-Agency #

Instructions on how to run this test task in local environment.

### Requirements? ###

* Docker
* Docker-Compose
* Free ports 3000 and 8080

### How do I get set up? ###

* CD to root of project
* run
```bash
docker-compose up
```

* Wait for script to finish with project setup (database setup, module installation, data migrations)
* Access project in browser on links below
* ???
* Enjoy

### Where can I access project via browser? ###

* Frontend: http://localhost:8080
* Backend API: http://localhost:3000/api

### Any users I can use to use to test the project? ###

#### Admin ####
* email: admin@q-agency.com
* password: 12345678

#### Authors ####
* email: dolor.Donec@purus.edu
* password: 12345678
---
* email: dolor.Donec@purus.edu
* password: 12345678
---
* email: augue@nasceturridiculus.org
* password: 12345678