import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Books from "@/views/Books.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/books",
    name: "Books",
    component: Books,
  },
  {
    path: "/authors",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Authors.vue"),
  },
  {
    path: "/login",
    name: "Login",
    props: true,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/LoginScreen.vue"),
  },
  { path: "/", redirect: { name: "Books" } },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
